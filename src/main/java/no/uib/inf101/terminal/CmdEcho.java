package no.uib.inf101.terminal;

import java.util.ArrayList;
import java.util.Arrays;

public class CmdEcho implements Command {

    @Override
    public String run(String[] args){
        String result = "";
        for ( int i= 0 ; i<args.length ; i++) {
            result += args[i] + " " ;

        }
        return result ;
    }


    @Override
    public String getName() {

        return "echo" ;
    }}

