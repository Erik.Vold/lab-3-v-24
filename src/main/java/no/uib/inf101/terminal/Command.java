package no.uib.inf101.terminal;

public interface Command {
    /**
     * Called when
     *
     * @param String[] args
     */
    String run(String[] args);

    /**
     * Called when
     *
     * @param
     *
     */
    String getName();

}
